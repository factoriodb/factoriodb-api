from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse
from rest_framework import serializers

from factoriodb_mods.models import Mod, Release, Screenshot, Category

User = get_user_model()


class UserModSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Mod
        fields = ('id', 'url', 'name', 'category', 'poster_image', 'releases')

class ModReleaseSerializer(serializers.HyperlinkedModelSerializer):
    
    class Meta:
        model = Release


class ModSerializer(serializers.HyperlinkedModelSerializer):

    views = serializers.IntegerField(read_only=True)
    downloads = serializers.IntegerField(read_only=True)
    releases = ModReleaseSerializer(read_only=True, many=True)

    class Meta:
        model = Mod


class ReleaseSerializer(serializers.ModelSerializer):

    class Meta:
        model = Release


class ScreenshotSerializer(serializers.ModelSerializer):

    class Meta:
        model = Screenshot

class CategoryModSeralizer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Mod

class CategorySerializer(serializers.ModelSerializer):

    mods = CategoryModSeralizer(read_only=True, many=True)

    class Meta:
        model = Category
        fields = ('id', 'name', 'created_at', 'updated_at', 'mod_count', 'mods')

class ListCategorySerializer(serializers.HyperlinkedModelSerializer):


    class Meta:
        model = Category
        fields = ('id', 'url', 'name', 'mod_count' )

    def mods_url(self, obj):
        return "{0}?category={1}".format(reverse('mod-list'), obj.id)

