from django.db import models
from django.conf import settings


class Category(models.Model):

	""" Model for a Category. """

	name = models.CharField(max_length=50)

	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	def __unicode__(self):
		return self.name

	def mod_count(self):

		""" Get number of mods in category """

		return self.mods.count()


class Mod(models.Model):

	""" Model for a Mod page. """

	class Meta:
		unique_together = (("owner", "name"))

	owner = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='mods')
	name = models.CharField(unique=True, max_length=50)
	poster_image = models.ImageField(blank=True, null=True)
	category = models.ForeignKey(Category, related_name='mods')

	description = models.TextField(blank=True, null=True)

	#analytics
	views = models.IntegerField(default=0)
	downloads = models.IntegerField(default=0)

	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	def __unicode__(self):
		return self.name


class Release(models.Model):

	""" Model for a Mod Release. """

	class Meta:
		unique_together = (("mod", "tagged_name"))

	tagged_name = models.CharField(max_length=50)
	mod = models.ForeignKey(Mod, related_name='releases')
	mod_file = models.FileField(blank=True, null=True)

	created_at = models.DateTimeField(auto_now_add=True)

	def __unicode__(self):
		return self.tagged_name


class Screenshot(models.Model):

	""" A model for mod screenshots. """

	user = models.ForeignKey(settings.AUTH_USER_MODEL, help_text='User who uploaded screenshot')
	mod = models.ForeignKey(Mod, help_text='The mod the screenshot belongs to')
	thumbnail = models.ImageField()
	screenshot = models.ImageField()

	created_at = models.DateTimeField(auto_now_add=True)

	def __unicode__(self):
		return "{0} screentshot - {1}".format(self.mod.name, self.id)