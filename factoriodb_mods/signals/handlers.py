from django.dispatch import receiver
from django.db.models.signals import post_save

import logging

logger = logging.getLogger('factorio.mods.signals')

UPDATES_MOD = (
	'Release',
	'Screenshot'
	)

@receiver(post_save)
def update_mod(sender, instance, *args, **kwargs):
	"""
	Reciever updates the updated_at field.
	Is only called on a mod when the post_save is called from a 
	matching model with mod as a foreignkey.
	"""
	if sender.__name__ in UPDATES_MOD:
		if hasattr(instance, 'updated_at'):
			instance.mod.updated_at = instance.updated_at
		else:
			instance.mod.updated_at = instance.created_at
		instance.mod.save()
