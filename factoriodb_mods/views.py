from django.contrib.auth import get_user_model
from rest_framework import viewsets, mixins, generics
from rest_framework_extensions.mixins import NestedViewSetMixin

from factoriodb_mods import serializers
from factoriodb_mods.models import Mod, Release, Screenshot, Category

User = get_user_model()

class UserModViewSet(NestedViewSetMixin, viewsets.ModelViewSet):

	""" API Endpoint for `mod` view nested inside a `user` endpoint. """

	queryset = Mod.objects.all()
	serializer_class = serializers.ModSerializer

class ModViewSet(viewsets.ModelViewSet):

	""" API endpoint for Mods. """

	queryset = Mod.objects.all()
	serializer_class = serializers.ModSerializer
	filter_fields = ('category', 'owner')

class CategoryModsViewSet(viewsets.ModelViewSet):

	""" API endpoint for mods belonging to a category """
	queryset = Mod.objects.all()

class ReleaseViewSet(viewsets.ModelViewSet):

	""" API endpoint for Releases. """

	queryset = Release.objects.all()
	serializer_class = serializers.ReleaseSerializer

class ScreenshotViewSet(viewsets.ModelViewSet):

	""" API endpoint for Screenshots. """

	queryset = Screenshot.objects.all()
	serializer_class = serializers.ScreenshotSerializer

class CategoryViewSet(viewsets.ModelViewSet):

	""" API endpoint for Categories. """

	queryset = Category.objects.all()

	def get_serializer_class(self):
		""" 
		Provide HyperLinkedModelSerializer for `list` view.
		"""
		if self.action == 'list':
			return serializers.ListCategorySerializer
		return serializers.CategorySerializer
