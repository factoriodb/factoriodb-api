# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('name', models.CharField(max_length=50)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='Mod',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('name', models.CharField(max_length=50, unique=True)),
                ('poster_image', models.ImageField(upload_to='', null=True, blank=True)),
                ('description', models.TextField(null=True, blank=True)),
                ('views', models.IntegerField(default=0)),
                ('downloads', models.IntegerField(default=0)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('category', models.ForeignKey(to='factoriodb_mods.Category')),
                ('owner', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='mods')),
            ],
        ),
        migrations.CreateModel(
            name='Release',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('tagged_name', models.CharField(max_length=50)),
                ('mod_file', models.FileField(upload_to='', null=True, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('mod', models.ForeignKey(to='factoriodb_mods.Mod', related_name='releases')),
            ],
        ),
        migrations.CreateModel(
            name='Screenshot',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('thumbnail', models.ImageField(upload_to='')),
                ('screenshot', models.ImageField(upload_to='')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('mod', models.ForeignKey(to='factoriodb_mods.Mod', help_text='The mod the screenshot belongs to')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, help_text='User who uploaded screenshot')),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='release',
            unique_together=set([('mod', 'tagged_name')]),
        ),
        migrations.AlterUniqueTogether(
            name='mod',
            unique_together=set([('owner', 'name')]),
        ),
    ]
