from django.apps import AppConfig


class FactorioModsConfig(AppConfig):
 
    name = 'factoriodb_mods'
    verbose_name = 'FactorioDB Mods'
 
    def ready(self):
 
        # import signal handlers
        import factoriodb_mods.signals.handlers