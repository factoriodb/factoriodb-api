# vim: set fileencoding=utf-8 :
from factoriodb_mods import models
from django.contrib import admin


class CategoryAdmin(admin.ModelAdmin):

    list_display = (u'id', 'name', 'created_at', 'updated_at')
    list_filter = ('created_at', 'updated_at')
    search_fields = ('name',)
    date_hierarchy = 'created_at'


class ModAdmin(admin.ModelAdmin):

    list_display = (
        u'id',
        'owner',
        'name',
        'poster_image',
        'category',
        'description',
        'views',
        'downloads',
        'created_at',
        'updated_at',
    )
    list_filter = ('owner', 'category', 'created_at', 'updated_at')
    search_fields = ('name',)
    date_hierarchy = 'created_at'


class ReleaseAdmin(admin.ModelAdmin):

    list_display = (u'id', 'tagged_name', 'mod', 'mod_file', 'created_at')
    list_filter = ('mod', 'created_at')
    date_hierarchy = 'created_at'


class ScreenshotAdmin(admin.ModelAdmin):

    list_display = (
        u'id',
        'user',
        'mod',
        'thumbnail',
        'screenshot',
        'created_at',
    )
    list_filter = ('user', 'mod', 'created_at')
    date_hierarchy = 'created_at'


def _register(model, admin_class):
    admin.site.register(model, admin_class)


_register(models.Category, CategoryAdmin)
_register(models.Mod, ModAdmin)
_register(models.Release, ReleaseAdmin)
_register(models.Screenshot, ScreenshotAdmin)
