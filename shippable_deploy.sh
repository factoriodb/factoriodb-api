#!/bin/bash

if [ "$BRANCH"  == "master" ]
then
    echo "Deploying master to dokku"
    git push -f dokku@remuria.net:factoriodb_api.git master
elif [ "$BRANCH" == "dev" ]
then
    echo "deploying dev"
else
    echo "No deployment for branch: $BRANCH"
fi