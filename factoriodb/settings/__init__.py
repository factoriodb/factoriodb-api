# Try to activate local settings. If it fails, assume we're on production and
# activate production settings. Note that local.py shouldn't be tracked in the
# repository.
import os

# If in CI environment, load test settings instead.
if "CI" in os.environ:
    from .ci import *
else:
    try:
        from .local import *
    except ImportError:
        from .prod import *
