from .base import *
import os
import dj_database_url

DEBUG = TEMPLATE_DEBUG = os.getenv('DEBUG', 'False')

DATABASES = { 'default': dj_database_url.config() }
DATABASES['default']['ENGINE'] = 'django_postgrespool'

# Opbeat config

INSTALLED_APPS += (
    "opbeat.contrib.django",
)

OPBEAT = {
    "ORGANIZATION_ID": "21c026c095694977af5f5d60453dedab",
    "APP_ID": "eda181e4ad",
    "SECRET_TOKEN": os.getenv('OPBEAT_SECRET_TOKEN')
}

MIDDLEWARE_CLASSES += (
    'opbeat.contrib.django.middleware.OpbeatAPMMiddleware',
)

STATICFILES_STORAGE = 'whitenoise.django.GzipManifestStaticFilesStorage'