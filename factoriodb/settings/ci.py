from .base import *
import dj_database_url

DATABASES = {'default': dj_database_url.parse('postgres://postgres@127.0.0.1:5432/factoriodb') }