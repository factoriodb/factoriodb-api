from django.conf.urls import patterns, include, url
from django.contrib import admin

from rest_framework_extensions.routers import ExtendedDefaultRouter

from factoriodb_auth.views import *
from factoriodb_mods.views import *

router = ExtendedDefaultRouter()

router.register(r'releases', ReleaseViewSet)

users_router = router.register(r'users', UserViewSet)
users_router.register(r'mods', UserModViewSet, 'users-mods', ['owner'])

mods_router = router.register(r'mods', ModViewSet)
mods_router.register(r'releases', ReleaseViewSet, 'mods-releases', ['mod'])

categories_router = router.register(r'catergories', CategoryViewSet)
categories_router.register(r'mods', ModViewSet, 'catergories-mods', ['category'])

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
)

from django.conf import settings

if settings.DEBUG:
	urlpatterns += patterns('',
	    (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
	)
