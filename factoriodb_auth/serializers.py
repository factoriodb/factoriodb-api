from django.contrib.auth import get_user_model
from rest_framework import serializers

from factoriodb_mods.serializers import UserModSerializer

User = get_user_model()

class UserSerializer(serializers.ModelSerializer):

    #mods = ModSerializer(many=True, blank=True, read_only=True)
    mods = UserModSerializer(read_only=True, many=True)
    class Meta:
        model = User
        fields = ('id', 'nickname', 'email', 'date_joined', 'mods')