# vim: set fileencoding=utf-8 :
from factoriodb_auth import models
from django.contrib import admin


class FactorioUserAdmin(admin.ModelAdmin):

    list_display = (
        u'id',
        'password',
        'last_login',
        'is_superuser',
        'email',
        'nickname',
        'is_staff',
        'is_active',
        'date_joined',
    )
    list_filter = (
        'last_login',
        'is_superuser',
        'is_staff',
        'is_active',
        'date_joined',
    )
    raw_id_fields = ('groups', 'user_permissions')


def _register(model, admin_class):
    admin.site.register(model, admin_class)


_register(models.FactorioUser, FactorioUserAdmin)
