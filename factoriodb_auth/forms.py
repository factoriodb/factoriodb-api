from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm

from factoriodb_auth.models import FactorioUser

class FactorioUserCreationForm(UserCreationForm):
    """
    A form that creates a user, with no privileges, from the given email and
    password.
    """
    def __init__(self, *args, **kargs):
        super(FactorioUserCreationForm, self).__init__(*args, **kargs)
        del self.fields['username']

    class Meta:
        model = FactorioUser
        fields = ('email', 'nickname')

class FactorioUserChangeForm(UserChangeForm):
    """
    A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """

    def __init__(self, *args, **kargs):
        super(FactorioUserChangeForm, self).__init__(*args, **kargs)
        del self.fields['username']

    class Meta:
        model = FactorioUser