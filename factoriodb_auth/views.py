from django.contrib.auth import get_user_model
from rest_framework import viewsets, mixins, generics
from rest_framework_extensions.mixins import NestedViewSetMixin

from factoriodb_auth import serializers

User = get_user_model()


class UserViewSet(viewsets.ModelViewSet):

	""" API endpoint for users. """

	queryset = User.objects.all()
	serializer_class = serializers.UserSerializer
