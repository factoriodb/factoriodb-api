Factorio API
=============

### Development Setup
1. ``` git clone git@bitbucket.org:kins/factoriodb-api.git ```
2. Install virtualenv and virtualenvwrapper
3. ``` mkvirtualenv factoriodb ```
4. ``` pip install -r requirements/dev.txt ```
5. ``` ./manage.py migrate ```
6. ``` ./manage.py runserver_plus ```

### Git Flow

We are using [git flow](https://github.com/nvie/gitflow/wiki/Installation) for managing releases and features.

You can find more information:
* http://jeffkreeftmeijer.com/2010/why-arent-you-using-git-flow/

When using git flow init, the defaults should be fine.

